// this js is used to show each panel. For now, they’re all in the same toolbar, but it would be better if we had one pane by tool
const icons = document.querySelectorAll('.icon');

icons.forEach(icon => icon.addEventListener('click', showpane)); 

  function showpane() {
    this.classList.toggle('active');
    const e = this.dataset.pane; 
    const f = document.querySelector(`#${e}`);
    console.log(e,f);
    document.querySelector(`#${e}`).classList.toggle('hide');
};







// drag the toolbar
   interact('.drag').draggable({
        inertia: false,
        onmove: function (event) {
          var target = event.target,
              // keep the dragged position in the data-x/data-y attributes
              x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
              y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
    
          // translate the element
          target.style.webkitTransform =
          target.style.transform =
            'translate(' + x + 'px, ' + y + 'px)';
    
          // update the position attributes
          target.setAttribute('data-x', x);
          target.setAttribute('data-y', y);
        },
        onend: function(event) {
            console.log(event);
        }
    }).allowFrom('.handle');




